//
//  ImageCell.swift
//  spotfinder
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var selectImageView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 1
    }
}
