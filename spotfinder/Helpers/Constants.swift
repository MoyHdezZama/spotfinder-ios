//
//  Constants.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation

struct K {
    
    #if RELEASE
    // release only code
    struct backendConstants {
        static let BaseURL = "https://spotfinder.cjapps.com.mx/"
        static let clientId = "1"
        static let clientSecret = "Gtm58SpI5mnYeCGM1eiFM1bJnYCPiYszgLn1DrpN"
    }
    #else
    // debug only code
    struct Server {
        static let BaseURL = "https://spotfinder.cjapps.com.mx/"
        static let clientId = "1"
        static let clientSecret = "Gtm58SpI5mnYeCGM1eiFM1bJnYCPiYszgLn1DrpN"
    }
    #endif
    
    struct Defaults {
        static let DateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        static let User = "user"
        static let Locale = "Locale"
        static let DeviceToken = "device_token"
        static let AccessToken = "access_token"
        static let RefreshToken = "refresh_token"
        static let TokenExpiresAt = "token_expires_at"
    }
    
    struct APIRoute {
        static let ALL = "api/spots"
        static let CREATE = "api/spots"
        static let GET_USER = "api/user"
        static let MY_SPOTS = "api/my-spots"
        static let REGISTER = "api/user"
        static let OAUTH = "oauth/token"
        static let FAVORITE = "api/spots/favorite"
        static let COMMENTS = "api/spot/comments"
        static let ADD_COMMENT = "api/spot/addComment"
    }
}
