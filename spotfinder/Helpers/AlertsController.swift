

//
//  AlertsController.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class AlertsController {
    
    static var TopController = AppDelegate.sharedInstance().topMostController()
    
    static func showAlert(delegate: UIViewController? = nil, title: String? = nil, message: String? = nil, OKButtonTitle: String, otherButtonTitle: String? = nil, success:(() -> ())? = nil, cancel:(() ->())? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: OKButtonTitle, style: .default) { (action) in
            if success != nil {
                success!()
            }
            
        }
        
        alertController.addAction(OKAction)
        
        if otherButtonTitle != nil {
            
            let cancelAction = UIAlertAction(title: otherButtonTitle, style: .default) { (action) in
                if cancel != nil {
                    cancel!()
                }
            }
            
            alertController.addAction(cancelAction)
        }

        if let vc = delegate {
            vc.present(alertController, animated: true, completion: nil)
        } else {
            TopController.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func showActionSheet(delegate: UIViewController? = nil, title: String? = nil, message: String? = nil, buttons: String..., handler: @escaping (_ buttonPressed: String) -> ()) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        for button in buttons {
            let button = UIAlertAction(title: button, style: .default) { (action) in
                handler(button)
            }
            alertController.addAction(button)
        }
        
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in }
        alertController.addAction(cancelar)
        
        if let vc = delegate {
            vc.present(alertController, animated: true, completion: nil)
        } else {
            TopController.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func showDeleteActionSheet(delegate: UIViewController? = nil, title: String = "", message: String = "", handler: @escaping (_ success: Bool) -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: "Eliminar", style: .destructive){ action in
            handler(true)
        }
        
        let CancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        alert.addAction(CancelAction)
        alert.addAction(DeleteAction)
        
        if let vc = delegate {
            vc.present(alert, animated: true, completion: nil)
        } else {
            TopController.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    static func showAlertWhitInput(delegate: UIViewController, title: String? = nil, message: String? = nil, placeHolder: String? = nil, OKButtonTitle: String, otherButtonTitle: String, success:((_ inputText: String) -> ())?, cancel:(() ->())? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            if placeHolder != nil {
                textField.placeholder = placeHolder!
            }
        }
        
        let OKAction = UIAlertAction(title: OKButtonTitle, style: .default) { (action) in
            if success != nil {
                if let textfields = alertController.textFields {
                    guard let inputText = textfields[0].text else {
                        return
                    }
                    
                    success!(inputText)
                }
                
            }
            
        }
        
        let cancelAction = UIAlertAction(title: otherButtonTitle, style: .default) { (action) in
            if cancel != nil {
                cancel!()
            }
        }

        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        
        delegate.present(alertController, animated: true, completion: nil)

    }
}
