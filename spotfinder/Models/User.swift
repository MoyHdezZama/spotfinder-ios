//
//  User.swift
//  spotfinder
//
//  Created by Moy Hdez on 24/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

struct User: Codable {
    
    var id: Int
    var name: String
    var email: String
    var image: URL
    var registrationMode: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case email
        case image
        case registrationMode = "registration_mode"
    }
    
    func isFromFacebook() -> Bool {
        return registrationMode == "facebook"
    }
    
    // MARK: - METHODS
    func saveToDefaults() {
        let encoder = JSONEncoder()
        let data = try! encoder.encode(self)
        UserDefaults.standard.set(data, forKey: K.Defaults.User)
    }
    
    public static func getFromDefaults() -> User? {
        guard let data = UserDefaults.standard.object(forKey: K.Defaults.User) as? Data else { return nil }
        let decoder = JSONDecoder()
        let user = try! decoder.decode(User.self, from: data)
        return user
    }
    
    static func destroy() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: K.Defaults.User)
        defaults.removeObject(forKey: K.Defaults.AccessToken)
        defaults.removeObject(forKey: K.Defaults.RefreshToken)
    }
}
