//
//  ImagePicker.swift
//  spotfinder
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class ImagePicker: NSObject {
    
    // MARK: Localized strings
    
    static let actionTitleCancel: String = NSLocalizedString("Cancel", comment: "")
    
    static let actionTitleTakePhoto: String = NSLocalizedString("Take photo", comment: "")
    
    static let actionTitleLibrary: String = NSLocalizedString("Select photo", comment: "")
    
    // MARK: Error's definition
    enum ErrorImagePicker: String {
        case CameraNotAvailable = "Camera not available"
        case LibraryNotAvailable = "Library not available"
        case AccessDeniedCameraRoll = "Access denied to camera roll"
        case EntitlementiCloud = "Missing iCloud Capatability"
        case WrongFileType = "Wrong file type"
        case PopoverTarget = "Missing property popoverTarget while iPad is run"
        case Other = "Other"
    }
    
    static func takePhoto<T: UIViewController>(vc: T) where T: UIImagePickerControllerDelegate & UINavigationControllerDelegate {

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = vc
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            imagePicker.mediaTypes = ["public.image"]
            vc.present(imagePicker,animated: true,completion: nil)
        } else {
            AlertsController.showAlert(delegate: vc, title: "", message: ErrorImagePicker.CameraNotAvailable.rawValue, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
    
    static func selectPhotoFromLibrary<T: UIViewController>(vc: T) where T: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = vc
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = ["public.image"]
            vc.present(imagePicker, animated: true, completion: nil)
        } else {
            AlertsController.showAlert(delegate: vc, title: "", message: ErrorImagePicker.LibraryNotAvailable.rawValue, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
    
    static func pickPhoto<T: UIViewController>(vc: T) where T: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: ImagePicker.actionTitleTakePhoto, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File Deleted")
            takePhoto(vc: vc)
        })
        
        let libraryAction = UIAlertAction(title: ImagePicker.actionTitleLibrary, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File Saved")
            selectPhotoFromLibrary(vc: vc)
        })
        
        let cancelAction = UIAlertAction(title: ImagePicker.actionTitleCancel, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(libraryAction)
        optionMenu.addAction(cancelAction)
        
        vc.present(optionMenu, animated: true, completion: nil)
    }
    
}
