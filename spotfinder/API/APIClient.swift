//
//  APIClient.swift
//  designado
//
//  Created by Moy Hdez on 25/04/17.
//  Copyright © 2017 CJAPPS. All rights reserved.
//

import UIKit
import Alamofire

typealias JSON = [String:Any]

class APIClient {
    
    enum Header: Int {
        case Default = 0
        case AccessToken = 1
        case Empty = 2
        
        var header: HTTPHeaders? {
            switch self {
            case .Default:
                guard let accessToken = UserDefaults.standard.value(forKey: K.Defaults.AccessToken) else { return ["Accept":"application/json","locale": Locale.current.languageCode ?? "es"] }
                return ["Accept":"application/json","Authorization":"Bearer \(accessToken)", "locale": Locale.current.languageCode ?? "es"]
            case .AccessToken:
                return ["Content-Type":"application/x-www-form-urlencoded", "locale": Locale.current.languageCode ?? "en"]
            case .Empty:
                return  ["Accept":"application/json","locale": Locale.current.languageCode ?? "en"]
            }
        }
    }
    
    func request(request: ApiRequest, completion: @escaping (_ response: ApiResponse, _ success: Bool) -> Void) {
        if let view = request.vc?.view {
            LilithProgressHUD.show(view)
        }
        
        Alamofire.request(K.Server.BaseURL+request.url, method: request.method, parameters: request.params, encoding: URLEncoding.default, headers: request.headers.header).responseJSON { (response) -> Void in
            
            if let view = request.vc?.view {
                LilithProgressHUD.hide(view)
            }
            
            var apiResponse = ApiResponse(error: ApiError(json: [
                "user": NSLocalizedString("We have encountered an internal error. Please try again.", comment: ""),
                "debug": "Server response is not valid"
                ]))
            
            if let json = response.value as? JSON {
                apiResponse = ApiResponse(json: json)
            }
            
            let statusCode = response.response?.statusCode ?? 500
            completion(apiResponse, (200...300).contains(statusCode))
            
            if statusCode == 401 {
                let vc = LoginNC.instance()
                request.vc?.present(vc, animated: true, completion: nil)
            }
            
            if request.showErrorAlert {
                self.handle(apiResponse, for: request.vc)
            }
        }
    }
    
    func handle(_ response: ApiResponse, for vc: UIViewController? ) {
        if let error = response.error, let vc = vc {
            AlertsController.showAlert(delegate: vc, title: nil, message: error.user, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
}

