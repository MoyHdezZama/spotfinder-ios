//
//  MapVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import GoogleMaps

class MapVC: UIViewController {
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var originNameLbl: UILabel!
    @IBOutlet var originAddressLbl: UILabel!
    @IBOutlet var labelsView: UIView!
    
    var delegate: UIViewController!
    
    var locationManager = CLLocationManager()
    var isFirstTime: Bool = true
    var location: Location!
    
    var isSelectingLocation: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        self.mapView.settings.myLocationButton = true
        self.mapView.isMyLocationEnabled = true
        
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString("Map", comment: "")
        if isSelectingLocation {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Ok", comment: ""), style: .done, target: self, action: #selector(selectLocation))
        }
    }
    
    @objc func selectLocation() {
        guard let vc = delegate as? CreateSpotVC else { return }
        let location = Location(title: originNameLbl.text!, subtitle: originAddressLbl.text!, coordinates: self.mapView.camera.target)
        vc.location = location
        vc.locationLbl.text = "\(location.title!), \(location.subtitle!)"
        self.navigationController?.popViewController(animated: true)
    }
    
    func setAddressLabels(title: String = "", subtitle: String = "") {
        UIView.animate(withDuration: 0.3) { 
            self.labelsView.isHidden = title.isEmpty && subtitle.isEmpty
        }
        self.originNameLbl.fadeTransition(duration: 0.1)
        self.originNameLbl.text = title
        self.originAddressLbl.fadeTransition(duration: 0.1)
        self.originAddressLbl.text = subtitle
    }
}

//MARK: - Map functions
extension MapVC {
    
    func animateCameraTo(_ location: CLLocationCoordinate2D) {
        if mapView == nil { return }
        let camera = GMSCameraPosition.camera(withTarget: location, zoom: 16)
        mapView.animate(to: camera)
    }
}

extension MapVC: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        GMSGeocoder().reverseGeocodeCoordinate(self.mapView.camera.target) { (response, error) in
            if error != nil {
                let alert = UIAlertController(title: nil, message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                if let address = response?.firstResult(), let lines = address.lines {
                    self.setAddressLabels(title: lines.isEmpty ? "" : lines[0], subtitle: lines.count < 2 ? "" : lines[1])
                }else{
                    self.setAddressLabels()
                }
            }
        }
    }
}

//MARK: - Location manager
extension MapVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if isFirstTime{
                animateCameraTo(self.location != nil ? self.location.coordinates : location.coordinate)
                isFirstTime = false
            }
        }
    }
}
