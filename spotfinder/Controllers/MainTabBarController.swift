//
//  MainTabBarController.swift
//  spotfinder
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is HomeNC {
            return true
        } else if User.getFromDefaults() == nil {
            let vc = LoginNC.instance()
            self.present(vc, animated: true, completion: nil)
            return false
        } else if viewController is NewSpotNC {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewSpotNav") as! UINavigationController
            self.present(vc, animated: true, completion: nil)
            return false
        } else {
            return true
        }
    }
}
