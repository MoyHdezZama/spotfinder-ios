//
//  RegisterVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import TextFieldEffects

class RegisterVC: UIViewController {
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var emailTxt: HoshiTextField!
    @IBOutlet weak var nameTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var confPassTxt: HoshiTextField!

    var newImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Sign up", comment: ""), style: .plain, target: self, action: #selector(validate))
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        imageContainer.setCornerRadius(4)
    }
    
    @IBAction func pickPhoto(_ sender: Any) {
        ImagePicker.pickPhoto(vc: self)
    }
    
    @objc func validate() {
        
        self.view.endEditing(true)
        
        var message = ""
        
        if emailTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter your email", comment: "")
        } else if nameTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter a name", comment: "")
        } else if passTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter the new password", comment: "")
        } else if confPassTxt.text!.isEmpty {
            message = NSLocalizedString("Please confirm the new password", comment: "")
        } else if passTxt.text != confPassTxt.text {
            message = NSLocalizedString("The passwords doesn't match", comment: "")
        } else {
            var params = [
                "email": emailTxt.text!,
                "name": nameTxt.text!,
                "password": passTxt.text!,
                "password_confirmation": confPassTxt.text!
            ]
            
            if let image = newImage {
                params.updateValue(image.base64(), forKey: "image")
            }
            
            register(params: params)
            return
        }
        
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
    }

}

// MARK: - IMAGEPICKER
extension RegisterVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else { print ("error") }
        
        if let image = newImage {
            profileIV.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("image picker cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - REQUESTS
extension RegisterVC {
    
    func register(params: JSON) {
        self.view.endEditing(true)
        APIClient().request(request: .Register(vc: self, params: params)) { [weak self] (response, success) in
            if success {
                guard let `self` = self, let userDic = response.data["user"] as? JSON,
                        let user = try? User(userDic)else { return }
                
                user.saveToDefaults()
                
                self.requestTokens()
            }
        }
    }
    
    func oauth(params: JSON, completion: @escaping (_ success: Bool) -> Void) {
        
        APIClient().request(request: .LoginByEmail(vc: self, params: params)) { (response ,success) in
            if success {
                UserDefaults.standard.setValue(response.data["access_token"] as? String, forKey: K.Defaults.AccessToken)
                UserDefaults.standard.setValue(response.data["refresh_token"] as? String, forKey: K.Defaults.RefreshToken)
                UserDefaults.standard.setValue(
                    Date().addingTimeInterval(response.data["expires_in"] as! Double).toStringWith(format: K.Defaults.DateFormat),
                    forKey: K.Defaults.TokenExpiresAt)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func requestTokens() {
        
        let params: JSON = [
            "grant_type": "password",
            "username": emailTxt.text!,
            "password": passTxt.text!,
            "client_id": K.Server.clientId,
            "client_secret": K.Server.clientSecret
        ]
        
        self.oauth(params: params, completion: { [weak self]  (success) in
            if success {
                let vc = MainTabBarController.instance()
                AppDelegate.sharedInstance().switchRootViewController(vc: vc, animated: true)
            } else {
                AlertsController.showAlert(delegate: self, title: "Error", message: NSLocalizedString("Registration was successful but there was an error while trying to login.", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), success: {
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        })
    }
}
