//
//  FilterVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var categoryTxt: UITextField!
    @IBOutlet weak var minDiffTxt: UITextField!
    @IBOutlet weak var maxDiffTxt: UITextField!
    
    var homeVC: HomeVC!

    var categories: [Category] = []
    var selectedCategory: Category?
    var minDiff = 1 {
        didSet{
            minDiffTxt.text = "\(self.minDiff)"
        }
    }
    var maxDiff = 5 {
        didSet{
            maxDiffTxt.text = "\(self.maxDiff)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Filter", comment: "")
        
        if let maxDistanceStr = homeVC.maxDistance, let maxDistance = Float(maxDistanceStr) {
            distanceSlider.value = maxDistance
            distanceLbl.text = "\(distanceSlider.value.rounded().removeDecimals) km"
        }
        
        if let minDiffStr = homeVC.params["min_difficulty"] as? String {
            self.minDiff = minDiffStr.toInt()
        }
        
        if let maxDiffStr = homeVC.params["max_difficulty"] as? String {
            self.maxDiff = maxDiffStr.toInt()
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Ok", comment: ""), style: .plain, target: self, action: #selector(filter))

        // Do any additional setup after loading the view.
    }
    
    func selectCategory() {
        let pickerVC = PickerVC.instance()
        var categoriesTitles = categories.map({$0.title})
        categoriesTitles.insert(NSLocalizedString("All categories", comment: ""), at: 0)
        pickerVC.list = [categoriesTitles]
        pickerVC.completion = { (section, row) in
            self.categoryTxt.text = categoriesTitles[row]
            if row == 0 {
                self.selectedCategory = nil
                return
            }
            self.selectedCategory = self.categories[row-1]
        }
        pickerVC.modalPresentationStyle = .overCurrentContext
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    func selectMinDiff() {
        let pickerVC = PickerVC.instance()
        pickerVC.list = [["1", "2", "3", "4", "5"]]
        var selected = 1
        pickerVC.completion = { (section, row) in
            selected = row + 1
        }
        
        pickerVC.onExit = {
            self.minDiff = selected
            if self.maxDiff < self.minDiff {
                self.maxDiff = self.minDiff
            }
        }
        pickerVC.modalPresentationStyle = .overCurrentContext
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    func selectMaxDiff() {
        let pickerVC = PickerVC.instance()
        pickerVC.list = [["1", "2", "3", "4", "5"]]
        var selected = 1
        pickerVC.completion = { (section, row) in
            selected = row + 1
        }
        
        pickerVC.onExit = {
            self.maxDiff = selected
            if self.minDiff > self.maxDiff {
                self.minDiff = self.maxDiff
            }
        }
        pickerVC.modalPresentationStyle = .overCurrentContext
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    @objc func filter() {
        var params: JSON = [
            "max_difficulty": maxDiff,
            "min_difficulty": minDiff,
        ]
        
        if let category = selectedCategory {
            params.updateValue(category.id, forKey: "category")
        }
        
        homeVC.maxDistance = distanceSlider.value.rounded().removeDecimals
        homeVC.params = params
        self.navigationController?.popViewController(animated: true) {
            self.homeVC.clearAndGetSpots()
            self.homeVC.clearAndGetMapSpots()
        }
    }

    @IBAction func distanceSliderChange(_ sender: Any) {
        distanceLbl.text = "\(distanceSlider.value.rounded().removeDecimals) km"
    }
    
    @IBAction func selectCategory(_ sender: Any) {
        guard !categories.isEmpty else { getCategories(); return }
        self.selectCategory()
    }
    
    @IBAction func selectMinDiff(_ sender: Any) {
        selectMinDiff()
    }
    
    @IBAction func selectMaxDiff(_ sender: Any) {
        selectMaxDiff()
    }
}

extension FilterVC {
    
    func getCategories() {
        APIClient().request(request: .Categories(vc: self)) { [weak self] (response, success) in
            if success {
                guard let `self` = self, let categoriesJson = response.data["categories"] as? [JSON], let categories = try? categoriesJson.map(Category.init) else { return }
                self.categories = categories
                self.selectCategory()
            }
        }
    }
}
