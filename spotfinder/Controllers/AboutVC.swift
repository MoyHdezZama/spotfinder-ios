//
//  AboutVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 05/04/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {
    
    @IBOutlet weak var summaryLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        summaryLbl.text = NSLocalizedString("App summary", comment: "")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func facebook(_ sender: Any) {
        let fbHooks = "fb://profile/688977087913712"
        let fbUrl = NSURL(string: fbHooks)
        if UIApplication.shared.canOpenURL(fbUrl! as URL)
        {
            UIApplication.shared.openURL(fbUrl! as URL)
        } else {
            UIApplication.shared.openURL(URL(string: "https://www.facebook.com/oBeja-Roller-688977087913712/")!)
        }
    }
    
    @IBAction func website(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "http://obejaroller.com/")!)
    }

}
